package com.devcamp.model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "grades")
public class CGrade {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	@JsonIgnore
	private CSubject subject;

	@ManyToOne
	@JsonIgnore
	private CStudent student;

	@NotNull
	@Column(name = "grade")
	private int grade;

	@NotNull
	@Column(name = "exam_date")
	private Date ExamDate;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", nullable = true, updatable = false)
	@CreatedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date createDate;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date", nullable = true)
	@LastModifiedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date updateDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CSubject getSubject() {
		return subject;
	}

	public void setSubjects(CSubject subject) {
		this.subject = subject;
	}

	public CStudent getStudent() {
		return student;
	}

	public void setStudents(CStudent student) {
		this.student = student;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public Date getExamDate() {
		return ExamDate;
	}

	public void setExamDate(Date examDate) {
		ExamDate = examDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
