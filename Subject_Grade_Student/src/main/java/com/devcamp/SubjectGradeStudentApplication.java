package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubjectGradeStudentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubjectGradeStudentApplication.class, args);
	}

}
