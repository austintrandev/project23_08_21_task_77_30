-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2021 at 07:44 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devcamp`
--

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) NOT NULL,
  `exam_date` datetime NOT NULL,
  `create_date` datetime NOT NULL,
  `grade` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `student_id` bigint(20) DEFAULT NULL,
  `subject_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `exam_date`, `create_date`, `grade`, `update_date`, `student_id`, `subject_id`) VALUES
(1, '2021-08-23 07:41:29', '2021-08-23 07:41:29', 8, '2021-08-23 07:41:29', 1, 1),
(2, '2021-08-23 07:41:29', '2021-08-23 07:41:29', 8, '2021-08-23 07:41:29', 2, 2),
(3, '2021-08-23 07:41:29', '2021-08-23 07:41:29', 10, '2021-08-23 07:41:29', 3, 3),
(4, '2021-08-23 07:41:29', '2021-08-23 07:41:29', 7, '2021-08-23 07:41:29', 4, 4),
(5, '2021-08-23 07:41:29', '2021-08-23 07:41:29', 6, '2021-08-23 07:41:29', 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `student_code` varchar(255) NOT NULL,
  `update_date` datetime NOT NULL,
  `user_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `create_date`, `first_name`, `last_name`, `student_code`, `update_date`, `user_name`) VALUES
(1, '2021-08-23 07:38:07', 'Ngô Thùy', 'Linh', 'stu001', '2021-08-23 07:38:07', 'linhnt'),
(2, '2021-08-23 07:38:07', 'Trần Đức', 'Trung', 'stu002', '2021-08-23 07:38:07', 'trungtd'),
(3, '2021-08-23 07:38:07', 'Nguyễn Ngọc Minh', 'Tâm', 'stu003', '2021-08-23 07:38:07', 'tamnnm'),
(4, '2021-08-23 07:38:07', 'Lê Đại', 'Hưng', 'stu004', '2021-08-23 07:38:07', 'hungld'),
(5, '2021-08-23 07:38:07', 'Trần Văn', 'Tuấn', 'stu005', '2021-08-23 07:38:07', 'tuantv');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `credit` int(11) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `create_date`, `credit`, `subject_code`, `subject_name`, `update_date`) VALUES
(1, '2021-08-23 07:33:37', 4, 'PHY1', 'Physic 1', '2021-08-23 07:33:37'),
(2, '2021-08-23 07:33:37', 4, 'PHY2', 'Physic 2', '2021-08-23 07:33:37'),
(3, '2021-08-23 07:33:37', 4, 'CHEM1', 'Chemistry 1', '2021-08-23 07:33:37'),
(4, '2021-08-23 07:33:37', 4, 'CHEM2', 'Chemistry 2', '2021-08-23 07:33:37'),
(5, '2021-08-23 07:33:37', 5, 'MATH', 'Math 1', '2021-08-23 07:33:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK13a16545m7vvrcspc999r15s9` (`student_id`),
  ADD KEY `FKrc0s5tgvm9r4ccxitaqtu88k5` (`subject_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_cgcf3r5xk73o0etbduc1qxnol` (`student_code`),
  ADD UNIQUE KEY `UK_174kjl69manglnf5695rrame3` (`user_name`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_qt734ivq9gq4yo4p1j1lhhk8l` (`subject_code`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `FK13a16545m7vvrcspc999r15s9` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `FKrc0s5tgvm9r4ccxitaqtu88k5` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
